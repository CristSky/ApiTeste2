﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dominio.Entities
{
    public class PedidoVm
    {
        public Cliente Cliente { get; set; }
        public List<Product> Produtos { get; set; }
    }
}
