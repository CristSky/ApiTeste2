﻿
using Dominio.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Testes
{
    [TestClass]
    public class ProdutoTeste
    {
        [TestMethod]
        public void TestaProduto()
        {
            var prod = new Product
            {
                Id = 1,
                Name = "teste",
                Category = "testando",
                Price = 0,
                Estoque = 20
            };
            Assert.AreEqual(true, prod.EmEstoque());
        }   
    }
}
