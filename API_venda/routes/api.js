var express = require('express');
var router = express.Router();
var models = require('./../models');

/* GET users listing. */
router
    .get('/products', function (req, res, next) {
        models.Product
            .findAndCountAll({
				attributes: ['id', 'name', 'category', 'price']
            })
            .then(function (data) {
                var json = [];

                for (var i = 0; i < data.count; i++) {
                    json.push(data.rows[i].dataValues);
                }
                res.setHeader('Content-Type', 'text/json; charset=utf-8');
                res.write(JSON.stringify(json, null, 4));
                res.end();
            })
            .catch(function (err) {
                console.error(err);
                res.status(404).send({msg: 'not found!'});
            });
    })
    .get('/products/:id', function (req, res) {
        models.Product
            .findOne({
                where: {
                    id: req.params.id
                }
            })
            .then(function (product) {
                if (product) {
                    res.setHeader('Content-Type', 'text/json; charset=utf-8');
                    res.write(JSON.stringify(product.toJSON(), null, 4));
                    res.end();
                } else res.status(404).send({msg: 'not found!'});
            })
            .catch(function (err) {
                console.error(err);
                res.status(404).send({msg: 'not found!'});
            });
    })
    .post('/products', function (req, res) {
        var product = {
            name: req.body.name,
            category: req.body.category,
            price: req.body.price
        };

        models.Product.create(product)
            .then(function (product) {
                res.status(201).send(product.toJSON());
            })
            .catch(function (err) {
                console.error(err);
                res.status(406).send({msg: 'not found!'});
            });
    })
    .put('/products/:id', function (req, res) {
        var data = {
            id: req.body.id,
            name: req.body.name,
            category: req.body.category,
            price: req.body.price
        };
        models.Product
            .findOne({
                where: {
                    id: req.params.id || product.id
                }
            })
            .then(function (product) {
                if (product) {
                    product.updateAttributes(data, ['name', 'category', 'price'])
                        .then(function (product) {
                            console.log(product);
                            res.status(202).send(product.toJSON());
                        })
                        .catch(function (err) {
                            console.error(err);
                        });
                } else res.status(404).send({msg: 'not found!'});
            })
            .catch(function (err) {
                console.error(err);
                res.status(404).send({msg: 'not found!'});
            });
    })
    .delete('/products/:id', function (req, res) {
        models.Product
            .destroy({
                where: {
                    id: req.params.id || req.body.id
                }
            })
            .then(function (status) {
                if (status)
                    res.status(202).send({status: 202});
                else res.status(404).send({msg: 'not found'});
            })
            .catch(function (err) {
                console.error(err);
                res.status(500).send({msg: 'error'});
            });
    });

module.exports = router;
