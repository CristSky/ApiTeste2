/**
 * Created by Cristiano on 26/05/2016.
 * https://github.com/CristSky
 */
'use strict';
module.exports = function (sequelize, DataTypes) {
    var Product = sequelize.define('Product', {
            name: {
                type: DataTypes.STRING,
                allowNull: false
            },
            category: {
                type: DataTypes.STRING
            },
            price: {
                type: DataTypes.DECIMAL(10, 2)
            }
        },
        {
            classMethods: {
                associate: function (models) {
                }
            }
        });
    return Product;
};